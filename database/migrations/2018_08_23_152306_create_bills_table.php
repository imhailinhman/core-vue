<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code' , 255)->nullable();
            $table->decimal('price', 16, 2)->default(0);
            $table->decimal('fee', 16, 2)->default(0);
            $table->decimal('bonus', 16, 2)->default(0);
            $table->decimal('extra', 16, 2)->default(0);
            $table->timestamp('date_order')->nullable();
            $table->string('payment',255)->nullable();
            $table->string('note',255)->nullable();
            $table->text('size')->nullable();
            $table->string('localtion_points',255)->nullable();
            $table->string('user_points',255)->nullable();
            $table->string('user_mobile',255)->nullable();
            $table->string('user_address',255)->nullable();
            $table->integer('user_id')->default(0)->unsigned();
            $table->tinyInteger('status', false, true)->default(1)->comment('0: Unactive , 1: active');
            $table->tinyInteger('type', false, true)->default(1)->comment('trạng thái hoàn thành đơn');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->engine = 'InnoDB';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bills');
    }
}
