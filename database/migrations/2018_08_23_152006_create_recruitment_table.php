<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruitment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position')->nullable();
            $table->string('alias')->nullable();
            $table->text('quantity')->nullable();
            $table->string('type')->nullable();
            $table->text('experience')->nullable();
            $table->text('salary')->nullable();
            $table->text('diploma')->nullable();
            $table->text('description')->nullable();
            $table->text('benefit')->nullable();
            $table->text('requirement')->nullable();
            $table->text('profile')->nullable();
            $table->text('image')->nullable();
            $table->text('thumb')->nullable();
            $table->dateTime('time_out')->nullable();
            $table->dateTime('time')->nullable();
            $table->integer('category_id', false, true)->default(0)->unsigned();
            $table->boolean('status')->default(false)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->engine = 'InnoDB';


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recruitment');
    }
}
