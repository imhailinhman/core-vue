<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class District
 * @package App\Models
 * @version October 30, 2018, 4:48 am UTC
 *
 * @property \App\Models\Province province
 * @property \Illuminate\Database\Eloquent\Collection payments
 * @property \Illuminate\Database\Eloquent\Collection promotionUses
 * @property string district_name
 * @property integer province_id
 * @property boolean status
 */
class District extends Model
{

    public $table = 'districts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'district_name',
        'province_id',
        'status',
        'slug'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'district_name' => 'string',
        'province_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function province()
    {
        return $this->belongsTo(\App\Models\Province::class);
    }
}
