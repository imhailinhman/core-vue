<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contact
 * @package App\Models
 * @version December 3, 2018, 10:48 pm +07
 *
 * @property \Illuminate\Database\Eloquent\Collection billDetail
 * @property \Illuminate\Database\Eloquent\Collection news
 * @property string name
 * @property string email
 * @property integer phone
 * @property string address
 * @property string note
 * @property string comment
 * @property boolean status
 */
class Contact extends Model
{
    use SoftDeletes;

    public $table = 'contacts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'note',
        'comment',
        'visa_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'integer',
        'address' => 'string',
        'note' => 'string',
        'comment' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required',
    ];

    public static $messages = [
        'name.required' => 'Tên là trường bắt buộc!',
        'phone.required' => 'Số điện thoại là trường bắt buộc!',
        'email.required' => 'Email là trường bắt buộc!',
    ];


    public function visa()
    {
        return $this->belongsTo('\App\Models\Visas', 'visa_id');
    }


}
