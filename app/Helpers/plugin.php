<?php

if (!function_exists('plugin')) {

    function plugin($name, $only = false)
    {
        if (is_array($name)) {
            $code = [];
            foreach ($name as $n) {
                $code[] = plugin($n, $only);
            }
            return implode("\n", $code);
        } else {
            //get plugin
            $path = "plugins/{$name}";
            $folder = public_path($path);
            $package = "{$folder}/package.json";
            if (!is_dir($folder) || !file_exists($package))
                throw new \League\Flysystem\Plugin\PluginNotFoundException("Can not find plugin {$package}");

            $package_data = json_decode(file_get_contents($package), true);

            //html to return
            $css = [];
            $js = [];
            if (isset($package_data['css'])) {
                foreach ($package_data['css'] as $file) {
                    $asset = str_replace(['\\/', '/\\'], '/', "{$path}/{$file}");
                    $url = asset($asset, is_secure());
                    $css[] = "<link href=\"{$url}\" rel=\"stylesheet\" type=\"text/css\"/>";
                }
            }
            if (isset($package_data['js'])) {
                foreach ($package_data['js'] as $file) {
                    $asset = str_replace(['\\/', '/\\'], '/', "{$path}/{$file}");
                    $url = asset($asset, is_secure());
                    $js[] = "<script src=\"{$url}\" type=\"text/javascript\"></script>";
                }
            }

            if ($only)
                return implode("\n", $$only);

            return implode("\n", array_merge($css, $js));
        }
    }
}