<?php

namespace App\Http\Controllers\Frontend;
use App\Events\ContactPusherEvent;
use App\Http\Controllers\AppBaseController;
use App\Models\Contact;
use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\ContactRepository;
use App\Repositories\Interfaces\FaqRepository;
use App\Repositories\Interfaces\NewRepository;
use App\Repositories\Interfaces\ProductImageRepository;
use App\Repositories\Interfaces\ProductRepository;
use App\Repositories\Interfaces\VisaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\SEOTools;


class HomeController extends AppBaseController
{
    private $newRepository;
    private $visaRepository;
    private $productRepository;
    private $productImageRepository;
    private $categoryRepository;
    private $faqRepository;
    private $contactRepository;

    public function __construct(
        NewRepository $newRepository,
        VisaRepository $visaRepository,
        CategoryRepository $categoryRepository,
        ProductRepository $productRepository,
        ProductImageRepository $productImageRepository,
        FaqRepository $faqRepository,
        ContactRepository $contactRepository

    )
    {
        $this->newRepository = $newRepository;
        $this->visaRepository = $visaRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->productImageRepository = $productImageRepository;
        $this->faqRepository = $faqRepository;
        $this->contactRepository = $contactRepository;
        parent::__construct();
    }

    public function index()
    {
        SEOMeta::setTitle('Tour du lịch trực tuyến số 1 tại Việt Nam | Du lịch địa cầu');
        SEOMeta::setDescription('');
        SEOMeta::setCanonical(config('app.url').'/trang-chu');
        SEOMeta::addKeyword(['dulichdiacau, du lich dia cau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia']);

        OpenGraph::setDescription('');
        OpenGraph::setTitle('Tour du lịch trực tuyến số 1 tại Việt Nam | Du lịch địa cầu');
        OpenGraph::setUrl(config('app.url'));
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle('Mạng bán tour trực tuyến số 1 tại Việt Nam | Du lịch địa cầu');
        TwitterCard::setSite('@dulichdiacau');

        $products = $this->productRepository->all(['*'], [], [], ['created_at' => 'DESC']);
        $news = $this->newRepository->all(['*'], [['status', '=', NewRepository::STATUS_ACTIVE]])->shuffle()->take(4);
        return view('frontend.index.home', compact('products', 'news'));
    }

    public function new(Request $request)
    {
        SEOMeta::setTitle('Tin tức du lịch địa cầu');
        SEOMeta::setDescription('');
        SEOMeta::setCanonical(config('app.url').'/trang-chu');
        SEOMeta::addKeyword(['Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia']);

        OpenGraph::setDescription('');
        OpenGraph::setTitle('Tin tức du lịch địa cầu');
        OpenGraph::setUrl(config('app.url').'/tin-tuc');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle('Mạng bán tour trực tuyến số 1 tại Việt Nam | Du lịch địa cầu');
        TwitterCard::setSite('@dulichdiacau');

        $limit = $request->input('limit', config('system.perPage'));
        $news = $this->newRepository->paginateList($limit, [['status', '=', NewRepository::STATUS_ACTIVE]], [], ['id' => 'DESC']);
        return view('frontend.new.index', compact('news'));
    }

    public function newInfo($slug)
    {
        $new = $this->newRepository->findBy('slug', $slug);
        SEOMeta::setTitle($new->title.'| DULICHDIACAU');
        SEOMeta::setDescription($new->description);
        SEOMeta::setCanonical(config('app.url').'/trang-chu');
        SEOMeta::addKeyword(['dulichdiacau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia']);

        OpenGraph::setDescription($new->description);
        OpenGraph::setTitle($new->title.'| DULICHDIACAU');
        OpenGraph::setUrl(config('app.url').'/tin-tuc');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle($new->title.'| DULICHDIACAU');
        TwitterCard::setSite('@dulichdiacau');

        $news = $this->newRepository->all(['*'], [['status', '=', NewRepository::STATUS_ACTIVE], ['slug', '!=', $slug]])->take(10);
        if (empty($new)) {
            return redirect(route('frontend.new.index'));
        }

        $products = $this->productRepository->all(['*'], [['status', '=', NewRepository::STATUS_ACTIVE],['category_id', '=', $new->category_id]])->shuffle()->take(3);
        return view('frontend.new.new_info', compact('new', 'news', 'products'));
    }

    public function tour(Request $request,$slug)
    {
        $category = $this->categoryRepository->findBy('slug', $slug);
        $title_and_keyword = $this->productRepository->getTitleAndKeyword($slug);

        SEOMeta::setTitle($title_and_keyword['title']);
        SEOMeta::setDescription($category->description);
        SEOMeta::setCanonical(config('app.url').'/'.$category->title_seo);
        SEOMeta::addKeyword([$title_and_keyword['keyword']]);

        OpenGraph::setDescription($category->description);
        OpenGraph::setTitle($title_and_keyword['title']);
        OpenGraph::setUrl(config('app.url').'/'.$category->title_seo);
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle($title_and_keyword['title']);
        TwitterCard::setSite('@dulichdiacau');


        $limit = $request->input('limit', config('system.perPage'));
        $products = $this->productRepository->paginateList($limit, [['category_id','=', $category->id], ['status', '=', ProductRepository::STATUS_ACTIVE]], [], ['created_at' => 'DESC']);
        $news = $this->newRepository->all(['*'], [['category_id','=', $category->id], ['status', '=', NewRepository::STATUS_ACTIVE]],[], ['created_at' => 'DESC'])->take(4);
        return view('frontend.tour.index', compact('products', 'category', 'news'));
    }

    public function tourInfo()
    {
        $route = Route::current()->parameters;
        $category = $this->categoryRepository->findBy('slug', $route['tour']);
        $product = $this->productRepository->findBy('title_seo', $route['slug']);
        SEOMeta::setTitle($category->name);
        SEOMeta::setDescription($category->description);
        SEOMeta::setCanonical(config('app.url').'/'.$category->title_seo);
        SEOMeta::addKeyword([$product->keyword_seo]);

        OpenGraph::setDescription($category->description);
        OpenGraph::setTitle($product->name);
        OpenGraph::setUrl(config('app.url').'/'.$category->title_seo);
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle($product->title);
        TwitterCard::setSite('@dulichdiacau');

        $product_interdepend = $this->productRepository->findAllBy('category_id', $category['id'])->where('id', '!=', $product->id)->take(8);

        $product_imgs = $this->productImageRepository->all(['*'], [['product_id','=', $product->id]]);
        return view('frontend.tour.tour_info', compact('product', 'product_imgs', 'route', 'product_interdepend', 'category'));
    }

    public function promotion(Request $request)
    {
        SEOMeta::setTitle('Tin tức khuyến mại');
        SEOMeta::setDescription('Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia');
        SEOMeta::setCanonical(config('app.url').'/chuong-trinh-khuyen-mai');
        SEOMeta::addKeyword(['dulichdiacau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia']);

        OpenGraph::setDescription('Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia');
        OpenGraph::setTitle('Tin tức du lịch địa cầu');
        OpenGraph::setUrl(config('app.url').'/chuong-trinh-khuyen-mai');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle('Mạng bán tour trực tuyến số 1 tại Việt Nam | Du lịch địa cầu');
        TwitterCard::setSite('@dulichdiacau');
        $limit = $request->input('limit', 2);
        $news = $this->newRepository->paginateList($limit, [['type', '=', NewRepository::TYPE_PROMOTION], ['status', '=', NewRepository::STATUS_ACTIVE]], [], ['id' => 'DESC']);
        return view('frontend.new.promotion', compact('news'));
    }

    public function promotionInfo($slug)
    {
        $news = $this->newRepository->all(['*'], [['status', '=', NewRepository::STATUS_ACTIVE], ['slug', '!=', $slug], ['type', '=', NewRepository::TYPE_PROMOTION]])->take(10);
        $new = $this->newRepository->findBy('slug', $slug);
        SEOMeta::setTitle($new->title.'| DULICHDIACAU');
        SEOMeta::setDescription($new->description);
        SEOMeta::setCanonical(config('app.url').'/chuong-trinh-khuyen-mai/'.$slug);
        SEOMeta::addKeyword([$new->keyword_seo]);

        OpenGraph::setDescription($new->description);
        OpenGraph::setTitle($new->title.'| DULICHDIACAU');
        OpenGraph::setUrl(config('app.url').'/chuong-trinh-khuyen-mai/'.$slug);
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage($new->image);
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle($new->title.'| DULICHDIACAU');
        TwitterCard::setSite('@dulichdiacau');
        if (empty($new)) {
            return redirect(route('frontend.new.index'));
        }

        $products = $this->productRepository->all(['*'], [['status', '=', NewRepository::STATUS_ACTIVE],['category_id', '=', $new->category_id]])->shuffle()->take(3);
        return view('frontend.new.promotion_info', compact('new', 'news', 'products'));
    }

    public function lienhe()
    {
        SEOMeta::setTitle('Liên hệ dulichdiacau');
        SEOMeta::setDescription('dulichdiacau, du lich dia cau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia');
        SEOMeta::setCanonical(config('app.url').'/lien-he');
        SEOMeta::addKeyword(['dulichdiacau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia']);

        OpenGraph::setDescription('dulichdiacau, du lich dia cau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia');
        OpenGraph::setTitle('Liên hệ dulichdiacau | Du lịch địa cầu');
        OpenGraph::setUrl(config('app.url').'/lien-he');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle('Liên hệ dulichdiacau | Du lịch địa cầu');
        TwitterCard::setSite('@dulichdiacau');
        return view('frontend.lienhe.index', compact(''));
    }

    public function postlienhe(Request $request)
    {
        $data = $request->only(['name', 'visa_id', 'email', 'phone', 'address', 'note', 'comment']);
        if (empty($data['name'])) {
            return $this->responseApp(false, 200, 'Tên là trường bắt buộc!');
        }
        if (empty($data['email'])) {
            return $this->responseApp(false, 200, 'Email là trường bắt buộc!');
        }
        if (empty($data['phone'])) {
            return $this->responseApp(false, 200, 'Số điện thoại là trường bắt buộc!');
        }

        if (empty($data['name'])) {
            return $this->responseApp(false, 200, 'Tên là trường bắt buộc!');
        }

        $data['visa_id'] = 0;
        if (!is_null($data['visa_id'])) {
            $data['visa_id'] = (int)$request->input('visa_id');
        }

        $data_save = $this->contactRepository->create($data);
        if ($data_save) {
            $count_id = $this->contactRepository->count(['status' => ContactRepository::STATUS_DEACTIVE]);
            event(new ContactPusherEvent($count_id,$data_save));
            return $this->responseApp(true, 200, 'Gửi Thành công!');
        } else {
            return $this->responseApp(false, 200, 'Gửi không thành công!');
        }
    }


    public function faq()
    {
        SEOMeta::setTitle('Câu hỏi thường gặp dulichdiacau');
        SEOMeta::setDescription('dulichdiacau, du lich dia cau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia');
        SEOMeta::setCanonical(config('app.url').'/trang-chu');

        OpenGraph::setDescription('dulichdiacau, du lich dia cau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia');
        OpenGraph::setTitle('Câu hỏi thường gặp dulichdiacau | Du lịch địa cầu');
        OpenGraph::setUrl(config('app.url'));
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle('Câu hỏi thường gặp dulichdiacau | Du lịch địa cầu');
        TwitterCard::setSite('@dulichdiacau');
        $faqs = $this->faqRepository->all(['*'], [['status', FaqRepository::STATUS_ACTIVE]], [], ['id' => 'DESC']);
        return view('frontend.faq.index', compact('faqs'));
    }

    public function search(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();

        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['name', 'like', $searchKey, 'OR'],
            ]];
        }
        if (!is_null($request->input('category'))) {
            $filter[] = ['category_id', (int)$request->input('category')];
        }

        if (!is_null($request->input('province'))) {
            $filter[] = ['end_point', (int)$request->input('province')];
        }
        if (!empty($filter)) {
            $products = $this->productRepository->paginateList($limit, $filter, [], ['created_at' => 'DESC']);
        } else {
            $products ='';
        }
        return view('frontend.search.index', compact('products', 'categores'));
    }


    public function visa(Request $request)
    {
        SEOMeta::setTitle('Tin tức du lịch địa cầu');
        SEOMeta::setDescription('');
        SEOMeta::setCanonical(config('app.url').'/trang-chu');
        SEOMeta::addKeyword(['Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia']);

        OpenGraph::setDescription('');
        OpenGraph::setTitle('Tin tức du lịch địa cầu');
        OpenGraph::setUrl(config('app.url').'/tin-tuc');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle('Mạng bán tour trực tuyến số 1 tại Việt Nam | Du lịch địa cầu');
        TwitterCard::setSite('@dulichdiacau');

        $limit = $request->input('limit', config('system.perPage'));
        $visas = $this->visaRepository->paginateList($limit, [['status', '=', NewRepository::STATUS_ACTIVE]], [], ['id' => 'DESC']);
        return view('frontend.visa.index', compact('visas'));
    }

    public function visaInfo($slug)
    {
        $visa = $this->visaRepository->findBy('slug', $slug);

        SEOMeta::setTitle($visa->title.'| DULICHDIACAU');
        SEOMeta::setDescription($visa->description);
        SEOMeta::setCanonical(config('app.url').'/trang-chu');
        SEOMeta::addKeyword(['dulichdiacau, Du lịch địa cầu, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia']);

        OpenGraph::setDescription($visa->description);
        OpenGraph::setTitle($visa->title.'| DULICHDIACAU');
        OpenGraph::setUrl(config('app.url').'/tin-tuc');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(config('app.url').'/layouts/images/logo.jpg');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'vn-us']);

        TwitterCard::setTitle($visa->title.'| DULICHDIACAU');
        TwitterCard::setSite('@dulichdiacau');

        if (empty($visa)) {
            return redirect(route('frontend.visa.index'));
        }

        return view('frontend.visa.visa_info', compact('visa'));
    }



    public function getPusher(){
        // gọi ra trang view demo-pusher.blade.php
        return view("frontend.lienhe.demo_pusher");
    }

    /**
     * @return string
     */
    public function fireEvent(){
        // Truyền message lên server Pusher
        event(new ContactPusherEvent("2"));
        return "Message has been sent.";
    }
}
