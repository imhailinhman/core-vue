<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Facades\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\Visas;
use App\Repositories\Interfaces\VisaRepository;
use App\Repositories\Interfaces\CategoryRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Response, URL;
use Validator;

class VisaController extends AppBaseController
{
    private $visaRepository;
    private $categoryRepository;
    public function __construct(
        VisaRepository $visaRepo,
        CategoryRepository $categoryRepository
    )
    {
        $this->visaRepository = $visaRepo;
        $this->categoryRepository = $categoryRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['title', 'like', $searchKey, 'OR'],
            ]];
        }

        if (!is_null($request->input('status'))) {
            $filter[] = ['status', (int)$request->input('status')];
        }
        $visas = $this->visaRepository->paginateList($limit, $filter, [], ['id' => 'DESC']);
        return view('backend.visas.index', compact('limit', 'visas'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();
        return view('backend.visas.create', compact('categores'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, Visas::$rules, Visas::$messages);
        $data = $request->only(
            [ 'title','description', 'slug', 'title_seo', 'content', 'description_seo', 'keyword_seo', 'category_id', 'user_id', 'status']);
        if (!isset($data['status'])) {
            $data['status'] = VisaRepository::STATUS_DEACTIVE;
        }

        $data['slug'] = Str::slug($data['title'], '-');
        $data['title_seo'] = Str::slug($data['title'], '-');
        $data['user_id'] = Auth::id();

        //Update for image
        if ($request->hasFile('image')) {
            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'visa');
        }

        try {
            $this->visaRepository->create($data);
            Flash::success('Taọ mới visa thành công! ');
            return redirect(route('visas.index'));
        } catch (\Exception $ex) {
            Flash::error('Tạo mới visa thất bại!');
            return redirect(route('visas.create'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();
        $model = $this->visaRepository->findById((int)$id);
        if (empty($model)) {
            Flash::error('Tin tức này không tồn tại!!');
            return redirect(route('visas.index'));
        }
        return view('backend.visas.edit', compact('model', 'categores'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, Visas::$rules, Visas::$messages);
        $model = $this->visaRepository->findById((int)$id);
        //check id visa not exist
        if (empty($model)) {
            Flash::error('Tin tức này không tồn tại!!');
            return redirect(route('visas.index'));
        }
        $data = $request->only(['title', 'description', 'slug', 'title_seo', 'content', 'description_seo', 'keyword_seo', 'category_id', 'user_id', 'status']);
        if (!isset($data['status'])) {
            $data['status'] = VisaRepository::STATUS_DEACTIVE;
        }
        if (!empty($request->input('title'))) {
            $data['slug'] = Str::slug($data['title'], '-');
            $data['title_seo'] = Str::slug($data['title'], '-');
        }

        $data['user_id'] = Auth::id();

        //Update for image
        if ($request->hasFile('image')) {
            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'visa');
        }
        try {
            $this->visaRepository->update($data, $id);
            Flash::success('Cập nhật visa thành công!');
            return redirect(route('visas.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật visa thất bại!');
            return redirect(route('visas.index'));
        }
    }

    //action all
    public function bulkActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $ids = explode(',', $input['ids']);
        $datas = $this->visaRepository->findByListId($ids);

        if ($datas->isEmpty()) {
            return $this->responseApp(false, 1, 'Người dùng không tồn tại!');
        }
        if ($key == 'delete') {
            foreach ($datas as $item) {
                if ($item->id == Auth::id()) {
                    return $this->responseApp(false, 1, 'Bạn không thể thao tác với chính mình!');
                } else {
                    $item->delete();
                }
            }
        } else if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        }

        return $this->responseApp(true, 200, 'successfully.');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->visaRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'Tin tức không tồn tại!');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }
}
