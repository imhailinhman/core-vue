<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Facades\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\Product;
use App\Repositories\Interfaces\DistrictRepository;
use App\Repositories\Interfaces\ProductImageRepository;
use App\Repositories\Interfaces\ProductRepository;
use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\ProvinceRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Response, URL;
use Validator;

class ProductController extends AppBaseController
{
    private $productRepository;
    private $productImageRepository;
    private $categoryRepository;
    private $districtRepository;
    private $provinceRepository;

    public function __construct(
        ProductRepository $productRepo,
        ProductImageRepository $productImageRepo,
        CategoryRepository $categoryRepository,
        DistrictRepository $districtRepository,
        ProvinceRepository $provinceRepository
    )
    {
        $this->productRepository = $productRepo;
        $this->productImageRepository = $productImageRepo;
        $this->categoryRepository = $categoryRepository;
        $this->districtRepository = $districtRepository;
        $this->provinceRepository = $provinceRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();

        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['name', 'like', $searchKey, 'OR'],
            ]];
        }
        if (!is_null($request->input('filter_status'))) {
            $filter[] = ['status', (int)$request->input('filter_status')];
        }

        if (!is_null($request->input('category'))) {
            $filter[] = ['category_id', (int)$request->input('category')];
        }

        $products = $this->productRepository->paginateList($limit, $filter, [], ['category_id' => 'DESC']);
        return view('backend.products.index', compact('limit', 'products', 'categores'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $transport = $this->productRepository->listTransport();
        $typeTour = $this->productRepository->listType();
        $district = $this->districtRepository->pluck('district_name', 'id')->toArray();
        $province = $this->provinceRepository->pluck('province_name', 'id')->toArray();
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();
        return view('backend.products.create', compact('categores', 'transport', 'district', 'province', 'typeTour'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, Product::$rules, Product::$messages);
        $data = $request->only([
            'name', 'type', 'style', 'alias', 'size', 'image', 'thumb', 'price', 'price_sale', 'title_seo',
            'description', 'description_seo', 'keyword_seo', 'content', 'follow', 'seat', 'type', 'status', 'category_id', 'province_id', 'district_id',
            'user_id', 'date_start', 'date_end', 'start_point', 'end_point', 'sum_time', 'transport', 'note', 'content_info',
        ]);

        if(!empty($data['date_start'])) {
            $data['date_start'] = Helper::formatDate($data['date_start'], 'Y-m-d');
        }

        if(!empty($data['date_end'])) {
            $data['date_end'] = Helper::formatDate($data['date_end'], 'Y-m-d');
        }

        $data['sum_time'] = Helper::showDiffTimeVn($data['date_end'], $data['date_start']);

        if (!isset($data['status'])) {
            $data['status'] = ProductRepository::STATUS_DEACTIVE;
        }

        if (!isset($data['type'])) {
            $data['type'] = ProductRepository::TYPE_DEFAULT;
        }

        if (!isset($data['seat'])) {
            $data['seat'] = $request->input('seat');
        }

        $data['follow'] = 0;

        if (!isset($data['transport'])) {
            $data['transport'] = ProductRepository::TRANSPORT_CAR;
        }

        $data['slug'] = Str::slug($data['name'], '-');
        $data['title_seo'] = Str::slug($data['name'], '-');
        $data['user_id'] = Auth::id();

        if (empty($data['price_sale']) || empty($data['price'])) {
            $data['price_sale'] = 0;
        }

        //Update for image
        if ($request->hasFile('image')) {
            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'products');
        }
        $products_save  =  $this->productRepository->create($data);

        if (!empty($request->file('multiple_file'))) {
            $data_image = [];
            foreach ( $request->file('multiple_file') as $file) {
                $products_file = UploadHelper::uploadImgFileProduct($file, 'product_images');
                $data_image['product_id'] = $products_save['id'];
                $data_image['title'] = $file->getClientOriginalName();
                $data_image['image'] = $products_file;
                $this->productImageRepository->create($data_image);
            }
        }
        try {
            Flash::success('Taọ mới tour thành công! ');
            return redirect(route('products.index'));
        } catch (\Exception $ex) {
            Flash::error('Tạo mới tour thất bại!');
            return redirect(route('products.create'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {

        $product = $this->productRepository->findById((int)$id);
        if (empty($product)) {
            Flash::error('Tour này không tồn tại!!');
            return redirect(route('backend.products.index'));
        }
        return view('backend.products.show', compact(  'product'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $transport = $this->productRepository->listTransport();
        $typeTour = $this->productRepository->listType();
        $district = $this->districtRepository->pluck('district_name', 'id')->toArray();
        $province = $this->provinceRepository->pluck('province_name', 'id')->toArray();
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();
        $product_images = $this->productImageRepository->all(['image', 'id'],[['product_id','=',$id]])->pluck('image', 'id')->toArray();
        $model = $this->productRepository->findById((int)$id);
        if (empty($model)) {
            Flash::error('Tour này không tồn tại!!');
            return redirect(route('products.index'));
        }
        return view('backend.products.edit', compact('model', 'categores', 'transport', 'district', 'province', 'typeTour', 'product_images'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, Product::$rules, Product::$messages);
        $model = $this->productRepository->findById((int)$id);

        if (empty($model)) {
            Flash::error('Tour này không tồn tại!!');
            return redirect(route('products.index'));
        }
        $data = $request->only([
            'name', 'type', 'style', 'alias', 'size', 'image', 'thumb', 'price', 'price_sale', 'title_seo',
            'description', 'description_seo', 'keyword_seo', 'content', 'follow', 'seat', 'type', 'status', 'category_id', 'province_id', 'district_id',
            'user_id', 'date_start', 'date_end', 'start_point', 'end_point', 'sum_time', 'transport', 'note', 'content_info',
        ]);

        if(!empty($data['date_start'])) {
            $data['date_start'] = Helper::formatDate($data['date_start'], 'Y-m-d');
        }

        if(!empty($data['date_end'])) {
            $data['date_end'] = Helper::formatDate($data['date_end'], 'Y-m-d');
        }

        $data['sum_time'] = Helper::showDiffTimeVn($data['date_end'], $data['date_start']);

        if (!isset($data['status'])) {
            $data['status'] = ProductRepository::STATUS_DEACTIVE;
        }

        if (!isset($data['type'])) {
            $data['type'] = ProductRepository::TYPE_DEFAULT;
        }

        if (!isset($data['seat'])) {
            $data['seat'] = $request->input('seat');
        }


        if (!isset($data['transport'])) {
            $data['transport'] = ProductRepository::TRANSPORT_CAR;
        }

        $data['slug'] = Str::slug($data['name'], '-');
        $data['title_seo'] = Str::slug($data['name'], '-');
        $data['user_id'] = Auth::id();

        if (empty($data['price_sale']) || empty($data['price'])) {
            $data['price_sale'] = 0;
        }

        //Update for image
        if ($request->hasFile('image')) {
            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'products');
        }
        if (!empty($request->file('multiple_file'))) {
            $data_image = [];
//            $product_image = $this->productImageRepository->findBy('product_id',$model['id']);
//            if(!empty($product_image)) {
//                $product_image->delete();
//
//            }
            foreach ( $request->file('multiple_file') as $file) {
                $products_file = UploadHelper::uploadImgFileProduct($file, 'product_images');
                $data_image['product_id'] = $model['id'];
                $data_image['title'] = $file->getClientOriginalName();
                $data_image['image'] = $products_file;
                $this->productImageRepository->create($data_image);
            }
        }

        try {
            $this->productRepository->update($data, $id);
            Flash::success('Cập nhật tour thành công!');
            return redirect(route('products.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật tour thất bại!');
            return redirect(route('products.index'));
        }
    }

    //action all
    public function bulkActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $ids = explode(',', $input['ids']);
        $datas = $this->productRepository->findByListId($ids);

        if ($datas->isEmpty()) {
            return $this->responseApp(false, 1, 'Sản phẩm không tồn tại!');
        }
        if ($key == 'delete') {
            foreach ($datas as $item) {
                $item->delete();
            }
        } else if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();
        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->productRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'Sản phẩm không tồn tại!');
        }
        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }


    public function updateProductImage(Request $request)
    {
//        if (!empty($request->file('multiple_file'))) {
//            $data_image = [];
//            foreach ( $request->file('multiple_file') as $file) {
////                $products_file = UploadHelper::uploadImgFile($file, 'products');
////                $data_image['title'] = $file->getClientOriginalName();
////                $data_image['link'] = $products_file;
////                $data_image['image'] = $products_file;
////                $this->productImageRepository->create($data_image);
//            }
//        }
//

        return $this->responseApp(true, 200, 'Thành công!');
    }


    public function deleteProductImage($id)
    {
        $data = $this->productImageRepository->findById($id);
        $data->delete();
        Helper::removeFile($data->title, config('filepath.product_images'));

        return $this->responseApp(true, 200, 'Thành công!');
    }
}
