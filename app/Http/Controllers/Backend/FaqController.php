<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Facades\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\Faq;
use App\Models\Faqs;
use App\Repositories\Interfaces\FaqRepository;
use App\Repositories\Interfaces\CategoryRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Response, URL;
use Validator;

class FaqController extends AppBaseController
{
    private $faqRepository;
    private $categoryRepository;

    public function __construct(
        FaqRepository $faqRepo,
        CategoryRepository $categoryRepository
    )
    {
        $this->faqRepository = $faqRepo;
        $this->categoryRepository = $categoryRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['title', 'like', $searchKey, 'OR'],
            ]];
        }

        if (!is_null($request->input('filter_status'))) {
            $filter[] = ['status', (int)$request->input('filter_status')];
        }
        $faqs = $this->faqRepository->paginateList($limit, $filter, [], ['id' => 'DESC']);
        return view('backend.faq.index', compact('limit', 'faqs'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.faq.create');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, Faq::$rules, Faq::$messages);
        $data = $request->only(
            [ 'title','description', 'status']);
        if (!isset($data['status'])) {
            $data['status'] = FaqRepository::STATUS_DEACTIVE;
        }

//        try {
            $this->faqRepository->create($data);
            Flash::success('Taọ mới câu hỏi thành công! ');
            return redirect(route('faq.index'));
//        } catch (\Exception $ex) {
//            Flash::error('Tạo mới câu hỏi thất bại!');
//            return redirect(route('faq.create'));
//        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $faq = $this->faqRepository->findById((int)$id);
        if (empty($faq)) {
            Flash::error('Câu hỏi này không tồn tại!!');
            return redirect(route('backend.faq.index'));
        }
        return view('backend.faq.show', compact(  'faq'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();
        $model = $this->faqRepository->findById((int)$id);
        if (empty($model)) {
            Flash::error('câu hỏi này không tồn tại!!');
            return redirect(route('faq.index'));
        }
        return view('backend.faq.edit', compact('model', 'categores'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, Faq::$rules, Faq::$messages);
        $model = $this->faqRepository->findById((int)$id);
        //check id faq not exist
        if (empty($model)) {
            Flash::error('faq này không tồn tại!!');
            return redirect(route('faq.index'));
        }
        $data = $request->only(['title', 'description', 'status']);
        if (!isset($data['status'])) {
            $data['status'] = FaqRepository::STATUS_DEACTIVE;
        }

        try {
            $this->faqRepository->update($data, $id);
            Flash::success('Cập nhật faq thành công!');
            return redirect(route('faq.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật faq thất bại!');
            return redirect(route('faq.index'));
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->faqRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'Câu hỏi không tồn tại!');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }
}
