<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\AppBaseController;
use App\Repositories\Interfaces\ContactRepository;
use Illuminate\Http\Request;
use Flash;
use Response;

class ContactController extends AppBaseController
{
    private $contactRepository;

    public function __construct(
        ContactRepository $contactRepository

    )
    {
        $this->contactRepository = $contactRepository;
        parent::__construct();
    }

    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        $contacts = $this->contactRepository->paginateList($limit, [], [], ['id' => 'DESC']);
        return view('backend.contacts.index', compact('limit', 'contacts'));
    }

    public function show($id)
    {

        $contact = $this->contactRepository->findById((int)$id);
        if (empty($contact)) {
            Flash::error('Thông tin liên hệ này không tồn tại!!');
            return redirect(route('contacts.index'));
        }

        if ($contact->status == ContactRepository::STATUS_DEACTIVE) {
            $contact->status = ContactRepository::STATUS_ACTIVE;
            $contact->save();
        }

        return view('backend.contacts.show', compact(  'contact'));
    }

    //action all
    public function bulkActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $ids = explode(',', $input['ids']);
        $datas = $this->contactRepository->findByListId($ids);

        if ($datas->isEmpty()) {
            return $this->responseApp(false, 1, 'Liên hệ không tồn tại!');
        }
        if ($key == 'delete') {
            foreach ($datas as $item) {
                if ($item->id == Auth::id()) {
                    return $this->responseApp(false, 1, 'Bạn không thể thao tác với chính mình!');
                } else {
                    $item->delete();
                }
            }
        } else if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        }

        return $this->responseApp(true, 200, 'successfully.');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->contactRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'Liên hệ không tồn tại!');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }

}
