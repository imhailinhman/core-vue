<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $models = [
            'Admin',
            'Role',
            'User',
            'New',
            'Category',
            'Product',
            'ProductImage',
            'Menu',
            'District',
            'Slide',
            'Province',
            'Contact',
            'Visa',
            'Faq'
        ];
        foreach ($models as $model) {
            $this->app->bind('App\Repositories\Interfaces\\' . $model . 'Repository', 'App\Repositories\Caches\Cache' . $model . 'Repository');
        }
    }
}
