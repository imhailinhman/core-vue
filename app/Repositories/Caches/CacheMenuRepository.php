<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbMenuRepository;
use App\Repositories\Interfaces\MenuRepository;

class CacheMenuRepository extends CacheRepository implements MenuRepository
{
    function __construct(DbMenuRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

    public function parentMenu($data, $parent = 0, $str="--", $select = 0)
    {
        return $this->dbRepository->parentMenu($data, $parent, $str, $select);
    }

}
