<?php

namespace App\Repositories\Interfaces;


interface ProductImageRepository  extends BaseRepository
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;
}
