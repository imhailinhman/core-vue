<?php

namespace App\Repositories\Interfaces;


interface DistrictRepository extends BaseRepository
{
    /**
     * @param $slug
     * @param $province_id
     * @return mixed
     */
    public function getIdBySlug($slug, $province_id);
}