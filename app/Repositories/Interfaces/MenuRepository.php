<?php

namespace App\Repositories\Interfaces;


interface MenuRepository  extends BaseRepository
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    function parentMenu($data, $parent = 0, $str="--", $select = 0);
}
