<?php

namespace App\Repositories\Eloquents;


use App\Models\Province;
use App\Repositories\Interfaces\ProvinceRepository;
use Illuminate\Support\Facades\DB;

class DbProvinceRepository extends DbRepository implements ProvinceRepository
{
    function __construct(Province $model)
    {
        $this->model = $model;
    }

    /**
     * @param $slug
     * @return bool
     */
    public function getIdBySlug($slug)
    {
        $item = $this->getBySlug($slug);
        if ($item) {
            return $item->id;
        } else {
            return false;
        }
    }

    /**
     * @param $slug
     * @return bool
     */
    public function getBySlug($slug)
    {
        return $this->model->where('slug', $slug)
            ->orWhere(DB::raw('REPLACE(slug, "-", "")'), $slug)
            ->first();
    }
}