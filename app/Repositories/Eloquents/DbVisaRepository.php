<?php

namespace App\Repositories\Eloquents;


use App\Models\Visas;
use App\Repositories\Interfaces\VisaRepository;

class DbVisaRepository extends DbRepository implements VisaRepository
{
    function __construct(Visas $model)
    {
        $this->model = $model;
    }

}
