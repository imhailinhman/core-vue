<?php

return [
    'add_bank_account' => 'Thêm tài khoản',
    'cancel' => 'Hủy',
    'delivery' => 'Giao',
    'insert' => 'Thêm mới',
    'looking_for' => 'Đang tìm...',
    'missing_phone_number_param' => 'Thiếu tham số số điện thoại',
    'order_cancel' => 'Đã hủy',
    'order_delete' => 'Hủy đơn',
    'order_find_shipper' => 'Tìm shipper',
    'order_running' => 'Đang chạy...',
    'order_shipper_accept' => 'Đã nhận đơn',
    'order_shipper_money_receive' => 'Shipper đã nhận tiền',
    'order_shipper_money_receive_desc' => 'Shipper đã hoàn tất việc nhận tiền của user',
    'order_shipper_qr_receive' => 'Đã nhận tiền<br>đang chờ quét QR',
    'order_success' => 'Hoàn thành',
    'order_success_note' => 'Đơn hàng thành công',
    'order_upload_image' => 'Tải ảnh chứng từ',
    'receive' => 'Nhận',
    'shipper' => 'Shipper',
    'transferred' => 'Đã chuyển khoản',
    'user_not_found' => 'Không tìm thấy thông tin người dùng'
];
