
<!-- Title Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('title', 'Tiêu đề:') !!}
    <span class="text-danger">(*)</span>
    {!! Form::text('title', (!empty($model)) ? $model->title : old('title'), ['placeholder'=>'Tiêu đề', 'class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Tóm tắt:') !!}
    {!! Form::textarea('description', (!empty($model)) ? $model->description : old('description'), ['placeholder'=>'Tóm tắt', 'class' => 'form-control', 'id' => 'editor1', 'rows' => 2]) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12 col-xs-12 col-md-12">
    {!! Form::label('status', 'Trạng thái :') !!}
    <div class="material-switch float-left">
        {!! Form::hidden('status', false) !!}
        <?php
        $array_atr = ['id' => 'someSwitchOptionSuccess'];
        if (!empty($model) && $model != '') {
            if ($model->status == 1) {
                $array_atr['checked'] = 'checked';
            }
        } else {
            $array_atr['checked'] = 'checked';
        }
        ?>
        {!! Form::checkbox('status', 1, null, $array_atr) !!}
        <label for="someSwitchOptionSuccess" class="label-success"></label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('faq.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script>
        $('#category_id').select2({
            placeholder: '---Tất cả---'
        });

        $('#formid').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(function () {
            CKEDITOR.replace('editor1',{
                height: '200px',
                language:'vi',
                filebrowserBrowseUrl :'/plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                customConfig: '/plugins/ckeditor/config.js'
            });
        })
    </script>
@endsection
