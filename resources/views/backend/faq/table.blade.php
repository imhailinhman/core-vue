<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100"
       cellspacing="0" width="100%" id="example">
    <thead>
    <tr>
        <th class="text-left nosort">Stt</th>
        <th class="text-left nosort">Tiêu đề</th>
        <th class="text-left nosort">Trạng thái</th>
        <th class="text-left nosort">Hành động</th>
    </tr>
    </thead>
    <tbody class="break-word">
    @php $count_number = (request('page', 1) - 1) * $limit; @endphp
    @foreach($faqs as $k => $faq)
        @php
            $count_number++;
        @endphp
        <tr id="tr-{!! $faq->id !!}">
            <td align="left">{{ $count_number }}</td>
            <td align="left">{{ !empty($faq->title) ? $faq->title : '' }}</td>
            <td class="text-left">
                @if($faq->status == 1)
                    <button type="button" class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="câu hỏi" data-text="vô hiệu" data-val="0"
                            data-id="{!! $faq->id !!}">Kích hoạt
                    </button>
                @else
                    <button type="button" class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="câu hỏi" data-text="kích hoạt" data-val="1"
                            data-id="{!! $faq->id !!}">Vô hiệu
                    </button>
                @endif
            </td>
            <td align="left">
                <div class='btn-group'>
                    <a href="{!! route('faq_f.index') !!}" class='btn btn-default btn-xs' target="_blank">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="{!! route('faq.edit', [$faq->id]) !!}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>

                    <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                       data-link="data-link-item-actions" data-key="delete" data-title="tin tức" data-text="Xóa" data-val="none" data-id="{!! $faq->id !!}">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

<div class="pagination-area text-center">
    @if(isset($faqs) && !empty($faqs))
        {!! $faqs->appends(request()->query())->links() !!}
    @endif
</div>
