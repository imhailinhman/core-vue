<div class="div_option">
    <div class="col-xs-12 col-md-4 col-sm-4 ">
        <div class="form-group">
            <input id="search_key" class="form-control search_name enter-submit" name="search_key" placeholder="Tìm kiếm ..." value="{{ request('search_key') }}"/>
        </div>
    </div>

    <div class="col-xs-12 col-md-4 col-sm-4 ">
        <div class="form-group">
            <div class="col-sm-10 col-xs-12 input-group">
                <select name="filter_status" class="form-control filter_header" id="status">
                    <option value=""> --- Lọc theo trạng thái --- </option>
                    <option value="1" {!! (request('filter_status') == '1') ? 'selected="selected"' : '' !!}>Kích hoạt</option>
                    <option value="0" {!! (request('filter_status') == '0') ? 'selected="selected"' : '' !!}>Vô hiệu </option>
                </select>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-4 col-sm-4 ">
        <div class="div_option wrap-filters ">
            <button class="btn btn-success form-group" type="submit">Tìm kiếm </button>
            <a class="btn btn-default" href="{{ route("faq.index") }}">Clear</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
