
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title') GUTINA</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    @include('backend.layouts.datatables_css')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @yield('css')
</head>

<body class="skin-blue sidebar-mini">
@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="/" class="logo">
                <h4>Trang chủ</h4>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flag-o"></i>
                                <span class="label label-danger counter-processing-order"></span>
                            </a>
                        </li>
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{!! URL::to('img/gutina.png') !!}"
                                     class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{!! Auth::user()->name !!}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="/img/logo_2.png"
                                         class="img-circle" alt="User Image"/>
                                    <p>
                                        {!! Auth::user()->name !!}
                                        {{--<small>Member since {!! Auth::user()->created_at->format('M. Y') !!}</small>--}}
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{!! route('admins.changepassword', [Auth::id()])!!}" class="btn btn-default btn-flat">Đổi mật khẩu </a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ url('admin/logout') }}" class="btn btn-default btn-flat"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Sign out
                                        </a>
                                        <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('backend.layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <div class="col-sm-12">
                <div class="breadcrumb bg-white">
                    <li><a href="{{ URL::to('/home/') }}"><i class="fa fa-home"></i>Trang điều khiển</a></li>
                    @yield('breadcrumb')
                </div>
            </div>
            <div class="clearfix"></div>
            @yield('content')
        </div>

        {{--<!-- Main Footer -->--}}
        {{--<footer class="main-footer" style="max-height: 100px;text-align: center">--}}
        {{--</footer>--}}

    </div>
@endif
@if(Auth::guest())
    <script type="text/javascript">
        window.location = "{{ route('auth.login') }}";//here double curly bracket
    </script>
@endif

@include('backend.layouts.datatables_js')
@yield('scripts')

<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script>
    $(document).ready(function(){
        // Khởi tạo một đối tượng Pusher với app_key
        var pusher = new Pusher('64f5fd6c2554fcab82b6', {
            cluster: 'ap1',
            encrypted: true
        });
        //Đăng ký với kênh chanel-real-time mà ta đã tạo trong file ContactPusherEvent.php
        var channel = pusher.subscribe('channel-real-time');
        //Bind một function addMesagePusher với sự kiện DemoPusherEvent
        channel.bind('App\\Events\\ContactPusherEvent', addData);
    });
    //function add message
    function addData(data) {
        var label = $("<span class='label label-danger ml-10'></span>");
        label.html(data.count);
        $('.count_contact').html(label);

        var table_contact = '<tr id="tr-'+ data.message.id +'">' +
            "            <td class='text-left p-t-0'>" +
            "                <div class='checkbox m-t-5'>" +
            "                    <label>" +
            "                        <input class='checkbox_item' type='checkbox' name='checkbox' value='1'>" +
            "                    </label>" +
            "                </div>" +
            "            </td>" +
            "            <td align='left'>"+ data.message.id +"</td>" +
            "            <td align='left'>"+ data.message.name +"</td>" +
            "            <td align='left'>"+ data.message.email +"</td>" +
            "            <td align='left'>"+ data.message.phone +"</td>" +
            "            <td align='left'>"+ data.message.address +"</td>" +
            "            <td class='text-left'>" +
            "                  <button type=\"button\" class=\"btn_status btn_status_false item_actions\">Chưa xem</button> " +
            "            </td>" +
            "            <td align='left'>" +
            "                <div class='btn-group'>" +
            "                    <a href='http://core-cms.local/admin/contacts/"+ data.message.id +"' class='btn btn-default btn-xs'><i class='glyphicon glyphicon-eye-open'></i></a>" +
            "                    <a href='javascript:void(0)' class='btn btn-danger btn-xs btn-edit item_actions btn-mg-2' " +
            "                    data-link='data-link-item-actions' data-key='delete' data-title='Liên hệ' " +
            "                    data-text='Xóa' data-val='none' data-id='"+ data.message.id +"'>" +
            "                    <i class='glyphicon glyphicon-trash'></i>" +
            "                    </a>" +
            "                </div>" +
            "            </td>" +
            "        </tr>";

        $('table#contacts tbody.contacts').append(table_contact);
    }
</script>


</body>
</html>
