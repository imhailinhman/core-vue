
<script src="{{ asset('plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/popper/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/adminlte/js/adminlte.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/icheck/icheck.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.buttons.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/colVis.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/moment/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-loading/src/loading.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/waitMe/js/waitMe.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/ckeditor/js/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/myjs.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/fileinput.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/theme.js') }}" type="text/javascript"></script>

{{--{!! plugin(--}}
{{--[--}}
{{--'jquery','popper','bootstrap','adminlte','icheck','select2','datatables.net',--}}
{{--'responsive','datatables.net-bs','moment','daterangepicker','bootstrap-datepicker',--}}
{{--'sweetalert','jquery-loading','waitMe','toastr','ckeditor','bootstrap-tagsinput'--}}
{{--]--}}
{{--, 'js') !!}--}}
{{--{!! js(['myjs.js', 'fileinput.min.js', 'theme.js']) !!}--}}

{{--<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') }}"></script>--}}
{{--<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js') }}"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/themes/fas/theme.min.js') }}"></script>--}}
