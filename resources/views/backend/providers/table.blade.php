<table class="table table-responsive" id="providers-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Alias</th>
        <th>Lang</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($providers as $provider)
        <tr>
            <td>{!! $provider->name !!}</td>
            <td>{!! $provider->alias !!}</td>
            <td>{!! $provider->lang !!}</td>
            <td>{!! $provider->status !!}</td>
            <td>
                {!! Form::open(['route' => ['providers.destroy', $provider->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('providers.show', [$provider->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('providers.edit', [$provider->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>