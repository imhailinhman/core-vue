@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="row">
            <div class="clearfix"></div>

            <input id="data-token" value="{{ csrf_token()}}" hidden><!-- viewing -->
            <input id="data-link-bulk-actions" value="{{ route("menus.bulkActions") }}" hidden>
            <input id="data-link-item-actions" value="{{ route("menus.itemActions") }}" hidden>

            <form action="" method="get" id="form_custom_list">
                @include('backend.menus.filter')
                <div class="div_option form-group mg-top-10">

                    <div class="col-xs-3 col-md-6 col-sm-6">
                        <ul class="nav navbar-left panel_toolbox">
                            <li class="dropdown">
                                <select class="form-control" name="limit" id="limit_pagination">
                                    <option @if (request('limit') == 10)selected="selected" @endif value="10">10</option>
                                    <option @if (request('limit') == 25)selected="selected" @endif value="25">25</option>
                                    <option @if (request('limit') == 50)selected="selected" @endif value="50">50</option>
                                    <option @if (request('limit') == 75)selected="selected" @endif value="75">75</option>
                                    <option @if (request('limit') == 100)selected="selected" @endif value="100">100</option>
                                </select>
                            </li>
                        </ul>
                    </div>


                    <div class="col-xs-9 col-md-6 col-sm-6 count_record text-right">
                        <a href="{{ route("menus.create") }}" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới menu
                        </a>
                        <a href="javascript:void(0);" value="none"
                           class="btn-toolbox btn btn-danger bulk_action_change"
                           data-link="data-link-bulk-actions" data-key="delete"
                           data-title="menu" data-text="xóa tất cả"><i class="glyphicon glyphicon-trash"></i>Xóa tất cả
                        </a>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Danh sách Menu </h3>
                </div>

                <div class="box-body">
                    @include('flash::message')
                    @include('backend.menus.table')
                </div>
            </div>
            <div class="text-center">

            </div>
        </div>

    </div>
@endsection