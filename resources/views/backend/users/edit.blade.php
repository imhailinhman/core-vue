@extends('backend.layouts.app')

@section('breadcrumb')
    <li><a href="{!! route('users.index') !!}">Danh sách khách hàng </a></li><li class="active">Cập nhật khách hàng {!! $model->fullname !!}</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Cập nhật khách hàng {!! $model->fullname !!}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::open(['route' => ['users.update', $model->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true , 'enctype' => 'multipart/form-data']) !!}

                   @include('backend.users.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
