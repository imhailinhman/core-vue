<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100"
       cellspacing="0" width="100%" id="example">
    <thead>
    <tr>
        <th class="text-left nosort">
            <label class="mb-0">
                <input type="checkbox" value="" id="select_all" name="select_all">
            </label>
        </th>
        <th class="text-left nosort">Stt</th>
        <th class="text-left nosort">Ảnh</th>
        <th class="text-left nosort">Tiêu đề</th>
        <th class="text-left nosort">Các tour</th>
        <th class="text-left nosort">Các loại tour</th>
        <th class="text-left nosort">Người viết bài</th>
        <th class="text-left nosort">Trạng thái</th>
        <th class="text-left nosort">Hành động</th>
    </tr>
    </thead>
    <tbody class="break-word">
    @php $count_number = (request('page', 1) - 1) * $limit; @endphp
    @foreach($products as $k => $product)
        @php
            $count_number++;
        @endphp
        <tr id="tr-{!! $product->id !!}">
            <td class="text-left p-t-0">
                <div class="checkbox m-t-5">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$product->id}}">
                    </label>
                </div>
            </td>
            <td align="left">{{ $count_number }}</td>

            <td align="left">
                <a target="_blank" href="{{ !empty($product->image) ? $product->image : '' }}">
                    <img src="{{ !empty($product->image) ? $product->image : '' }}" class="  img-fluid img-thumbnail center-block " width="30px" height="20px">
                </a>
            </td>
            <td align="left">{{ !empty($product->name) ? str_limit($product->name, $limit = 70, $end = '...')  : '' }}</td>
            <td align="left">{{ !empty($product->category) ? $product->category->title : '' }}</td>
            <td align="left">{{ !empty($product->type) ? $product->getTypeText() : '' }}</td>
            <td align="left">{{ !empty($product->admins) ? $product->admins->fullname : '' }}</td>
            <td class="text-left">
                @if($product->status == 1)
                    <button type="button" class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="sản phẩm" data-text="vô hiệu" data-val="0"
                            data-id="{!! $product->id !!}">Kích hoạt
                    </button>
                @else
                    <button type="button" class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="sản phẩm" data-text="kích hoạt" data-val="1"
                            data-id="{!! $product->id !!}">Vô hiệu
                    </button>
                @endif
            </td>
            <td align="left">
                <div class='btn-group'>
                    <a href="{!! config('app.url').'/'.$product->category->title_seo.'/'.$product->title_seo !!}" class='btn btn-default btn-xs' target="_blank">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="{!! route('products.edit', [$product->id]) !!}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>

                    <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                       data-link="data-link-item-actions" data-key="delete" data-title="sản phẩm" data-text="xóa" data-val="none" data-id="{!! $product->id !!}">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

<div class="pagination-area text-center">
    @if(isset($products) && !empty($products))
        {!! $products->appends(request()->query())->links() !!}
    @endif
</div>
