@extends('backend.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Product
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::open(['route' => ['products.update', $model->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true , 'enctype' => 'multipart/form-data', 'id' => 'formid']) !!}

                        @include('backend.products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
