@extends('backend.layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Thông tin liên hệ
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="form-group col-sm-12 col-xs-12 row text-left">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <!-- Profile Image -->

                        <div class="box-body box-profile">
                            <!-- Name Field -->

                            <div class="form-group col-sm-6 col-xs-6">
                                {!! Form::label('name', 'Name:') !!}
                                <p>{!! $contact->name !!}</p>
                            </div>

                            <!-- Email Field -->
                            <div class="form-group col-sm-6 col-xs-6">
                                {!! Form::label('email', 'Email:') !!}
                                <p>{!! $contact->email !!}</p>
                            </div>

                            <!-- Phone Field -->
                            <div class="form-group col-sm-6 col-xs-6">
                                {!! Form::label('phone', 'Số điện thoại:') !!}
                                <p>{!! $contact->phone !!}</p>
                            </div>

                            <!-- Address Field -->
                            <div class="form-group col-sm-6 col-xs-6">
                                {!! Form::label('address', 'Địa chỉ:') !!}
                                <p>{!! $contact->address !!}</p>
                            </div>

                            <!-- Note Field -->
                            <div class="form-group col-sm-6 col-xs-6">
                                {!! Form::label('note', 'Tiêu đề:') !!}
                                <p>{!! $contact->note !!}</p>
                            </div>

                            <!-- Comment Field -->
                            <div class="form-group  col-sm-12 col-xs-12">
                                {!! Form::label('comment', 'Nội dung:') !!}
                                <p>{!! $contact->comment !!}</p>
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-xs-12 row text-center">
                            <a href="{!! route('contacts.index') !!}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
