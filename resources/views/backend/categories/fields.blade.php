<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Tiêu đề:') !!}
    <span class="text-danger">(*)</span>
    {!! Form::text('title', (!empty($model)) ? $model->title : old('title'), ['placeholder'=>'Tiêu đề', 'class' => 'form-control']) !!}
</div>

<!-- company_id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('menu_id', 'Menu:') !!}
    <span class="text-danger">(*)</span>
    <select name="menu_id" id="menu_id" class="form-control select2">
        <option value="">-------- Menu --------</option>
        @if(!empty($menus))
            @foreach($menus as $k_id => $menu)
                <?php
                if (!empty($model)) {
                    $selected = ($k_id == $model->menu_id) ? 'selected' : null;
                } else {
                    $selected = '';
                }
                ?>
                <option {{$selected}} value="{{ $k_id }}" {{ (old("category_id") == $k_id ? "selected":"") }}>{{ $menu }}</option>
            @endforeach
        @endif
    </select>
</div>

{{--<!-- company_id Field -->--}}
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('parent_id', 'Danh mục:') !!}--}}
    {{--<span class="text-danger">(*)</span>--}}
    {{--<select name="parent_id" id="parent_id" class="form-control select2">--}}
        {{--<option value="">-------- Danh mục --------</option>--}}
        {{--@if(!empty($categories))--}}
            {{--{!! \App\Models\Category::parentCate($categories) !!}--}}
        {{--@endif--}}
    {{--</select>--}}
{{--</div>--}}

<!-- Keyword Seo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keyword_seo', 'Keyword Seo:') !!}
    {!! Form::text('keyword_seo', (!empty($model)) ? $model->keyword_seo : old('keyword_seo'), ['placeholder'=>'Keyword Seo', 'class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6 col-xs-12 col-md-6">
    {!! Form::label('status', 'Trạng thái :') !!}
    <div class="material-switch float-left">
        {!! Form::hidden('status', false) !!}
        <?php
        $array_atr = ['id' => 'someSwitchOptionSuccess'];
        if (!empty($model) && $model != '') {
            if ($model->status == 1) {
                $array_atr['checked'] = 'checked';
            }
        } else {
            $array_atr['checked'] = 'checked';
        }
        ?>
        {!! Form::checkbox('status', 1, null, $array_atr) !!}
        <label for="someSwitchOptionSuccess" class="label-success"></label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categories.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script>
        $('#menu_id').select2({
            placeholder: '---Tất cả---'
        });

        $('#formid').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

    </script>
@endsection
