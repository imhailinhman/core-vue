@extends('backend.layouts.app')

@section('breadcrumb')
    <li><a href="{!! route('admins.index') !!}">Danh sách quản trị</a></li><li class="active">Chi tiết quản trị {!! $admin->fullname !!}</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            {!! $admin->fullname !!}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                @include('backend.admins.show_fields')
                <!-- Submit Field -->
                    <div class="form-group col-sm-12 col-xs-12 row text-center">
                        <a href="{!! route('admins.index') !!}" class="btn btn-default">Quay lại </a>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
@endsection
