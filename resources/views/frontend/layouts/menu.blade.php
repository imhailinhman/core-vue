
@inject('menuHelper', 'App\Helpers\MenuHelper')

<header>
    <section id="up-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 logo">
                    <a href="{{asset('')}}"><img src="{{asset('')}}layouts/images/logo.jpg" alt="" class="logo"></a>
                </div>
                <div class="col-sm-9 info-up-head pull-right hidden-xs ">
                    <div class="block-content col-md-10 col-md-push-1 pull-right">
                        <div class=" col-sm-6">
                            <span class="ico-contactus col-sm-2 no-padding">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> </span>
                            <div class="inner col-sm-10 text-left"> Công ty TNHH Du Lịch Địa Cầu<br>
                                <strong>P604 Tòa Nhà 813 Giải Phóng, Giáp Bát, Hoàng Mai, Hà nội</strong>

                            </div>
                        </div>
                        <div class=" col-sm-4">
                            <span class="ico-contactus col-sm-2"><i class="fa fa-phone"></i></span>
                            <div class="inner col-sm-10 text-left "> Hotline<br>
                                <strong>+84-4-73000016</strong>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <nav class="navbar navbar-default mb-0" id="my-nav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse no-padding" id="main-menu">
                <ul class="nav navbar-nav menu-item">
                    @if(!empty($menuHelper->ListMenu()))
                        @foreach($menuHelper->ListMenu() as $menu)
                            <li class="dropdown"><a @if(!$menuHelper->ListCate($menu->id)->isEmpty()) data-toggle="dropdown" @endif href="/{{ $menu->route }}">{{ $menu->name }} <span class="sr-only">(current)</span></a>
                                @if(!$menuHelper->ListCate($menu->id)->isEmpty())
                                    <ul class="dropdown-menu multi-level drop-lv0" role="menu" aria-labelledby="dropdownMenu">
                                        @foreach($menuHelper->ListCate($menu->id) as $menu_item)
                                            <li><a href="/{{ $menu_item->slug }}">{{ $menu_item->title }}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

</header>
