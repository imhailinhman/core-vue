<footer id="footer2">
    <!-- footer paralax-->
    <div class="footer-paralax">
        <div class="footer-row">
            <div class="container">
                <div class="row footer-line-1">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mg-bot30">
                        <h3 class="ft-title">Liên hệ</h3>
                        <div class="mg-bot10">Công ty TNHH Du Lịch Địa Cầu</div>
                        <div class="mg-bot10">P604 Tòa Nhà 813 Giải Phóng, Giáp Bát, HOàng Mai, Hà nội</div>
                        <div class="mg-bot5">
                            <div class="f-left">
                                <img src="{{asset('')}}layouts/images/i-phone.png" alt="phone">
                            </div>
                            <div class="f-left ft-mg-ct ft-mgtop">
                                (+84 28) 99999999
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="mg-bot5">
                            <div class="f-left">
                                <img src="{{asset('')}}layouts/images/i-fax.png" alt="fax">
                            </div>
                            <div class="f-left ft-mg-ct ft-mgtop">
                                (+84 28) 99999999
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="mg-bot15">
                            <div class="f-left">
                                <img src="{{asset('')}}layouts/images/i-mail.png" alt="mail">
                            </div>
                            <div class="f-left ft-mg-ct ft-mgtop">
                                <a href="mailto:dulichdiacau2018@gmail.com" style="color:#337ab7;">dulichdiacau2018@gmail.com</a>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="mg-bot5">
                            <img src="{{asset('')}}layouts/images/map.png" style="border: 1px solid #e4e4e4;" class="img-responsive" alt="map">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-footer has-chevron">
                        <div class="widget-container">
                            <h3 class="widget-title">THÔNG TIN</h3>
                            <div class="widget-body">
                                <div class="row">
                                    <ul class="list-chinhsach">
                                        <li><a href="">dulichdiacau</a></li>
                                        <li><a href="">Chính sách riêng tư</a></li>
                                        <li><a href="">Thỏa thuận sử dụng</a></li>
                                        <li><a href="{{ route('faq_f.index') }}">Câu hỏi thường gặp</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-footer">
                        <div class="widget-container">
                            <div class="widget-body">
                                <h3>Đăng kí</h3>
                                <form id="form-subcribe" class="" method="post">
                                    <div class="wrap-form">
                                        <input class="form-control email-input" name="EMAIL" placeholder="Enter Your E-mail" required="" type="email">
                                        <!--<input id="stm_newsletter_submit" value="" type="submit">-->
                                        <button id="stm_newsletter_submit" value="" type="submit"><i class="fa fa-rss" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <p>Nhận thông tin cập nhật và ưu đãi mới nhất</p>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 mg-bot">
                        <div class="row">
                            <div class="col-lg-12 col-md-3 col-sm-4 col-xs-12">
                                <h3 class="ft-title">Mạng xã hội</h3>
                                <div class="mg-bot30">
                                    <div class="f-left">
                                        <a href="https://www.facebook.com/dulichdiacau/" rel="nofollow" target="_blank" title="facebook">
                                            <i class="iconT-fb"></i>
                                        </a>
                                    </div>
                                    <div class="f-left  ft-mg-sc">
                                        <a href="https://www.instagram.com/dulichdiacau/" rel="nofollow" target="_blank" title="instagram">
                                            <i class="iconT-instagram"></i>
                                        </a>
                                    </div>
                                    <div class="f-left  ft-mg-sc">
                                        <a href="https://twitter.com/dulichdiacau" rel="nofollow" target="_blank" title="twitter">
                                            <i class="iconT-tw"></i>
                                        </a>
                                    </div>
                                    <div class="f-left  ft-mg-sc">
                                        <a href="https://www.youtube.com/channel/UCY4diIi4ZvrcZIfpLTTM11g" rel="nofollow" target="_blank" title="youtube">
                                            <i class="iconT-yt"></i>
                                        </a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-3 col-sm-4 col-xs-12">
                                <h3 class="ft-title">Hotline</h3>
                                <div class="mg-bot30">
                                    <div class="f-left">
                                        <i class="iconT-i-hotline"></i>
                                    </div>
                                    <div class="f-left ft-mg-hl">
                                        <div class="hl-num"><a href="tel:1900 1839">1900 1839</a></div>
                                        <div class="hl-hour">Từ 08h00 - 23h00</div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- ./footer paralax-->
</footer>
