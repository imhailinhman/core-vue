<!DOCTYPE html>
<html>
<head>
    @include('frontend.layouts.meta_seo')
    @include('frontend.layouts.datatables_css')
    @yield('css_frontend')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
<div id="app">
    @include('frontend.layouts.menu')
    @include('frontend.layouts.slide')
    @include('frontend.layouts.search')
    @yield('content')
    @include('frontend.layouts.footer')
</div>
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '578734159326726',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v4.0'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script async defer src="https://connect.facebook.net/vi_VN/sdk.js"></script>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
         attribution=setup_tool
         page_id="113414180035647"
         logged_in_greeting="Xin chào!"
         logged_out_greeting="Xin chào!">
    </div>
    {{--<div--}}
        {{--class="fb-like"--}}
        {{--data-share="true"--}}
        {{--data-width="450"--}}
        {{--data-show-faces="true">--}}
    {{--</div>--}}
</body>
</html>

@include('frontend.layouts.datatables_js')
@yield('js_frontend')
