
@inject('slideHelper', 'App\Helpers\SlideHelper')

<div class="n3-slideshow hidden-xs">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        @php
            $slide_active = $slideHelper->ListSlide()->last()['id'];
        @endphp

        <ol class="carousel-indicators hidden">
            @if(!empty($slideHelper->ListSlide()))
                 @foreach($slideHelper->ListSlide() as $key => $slide)
                    <li data-target="#myCarousel1" data-slide-to="{{ $key }}" class="@if(!empty($slide_active) && $slide->id == $slide_active) active @endif"></li>
                @endforeach
            @endif
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            @if(!empty($slideHelper->ListSlide()))
                @foreach($slideHelper->ListSlide() as $slide)
                    <div class="item @if(!empty($slide_active) && $slide->id == $slide_active) active @endif">
                        <a href="{{ route('newInfo.index', ['id' => $slide->new->slug]) }}" title="{{ $slide->title }}">
                            <img alt="{{ $slide->title }}" src="{{ $slide->image }}" class="w-100 img-slide">
                        </a>
                    </div>
                @endforeach
            @endif
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control hidden-xs" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control hidden-xs" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
