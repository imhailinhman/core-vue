@extends('frontend.layouts.master')
@section('content')
<div class="container">



    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="{{ route('promotion.index') }}" itemprop="url"><span itemprop="title">Tin tức khuyến mại</span></a></span> »
                        <span><a href="{{ route('promotionInfo.index', ['id' => $new->slug]) }}" itemprop="url"><span itemprop="title">{{ $new->title }}</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container n3-news mg-bot40">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 l">
                <div class="frame-dt-top">
                    <h1 class="news-detail-title">
                        {{ $new->title }}
                    </h1>
                    <div class="frame-date">
                        <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                        <div class="f-left date">{{ date('d/m/Y', strtotime($new->created_at)) }}</div>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="frame-dt-bot">
                    {!! $new->description !!}
                </div>

                <div class="frame-dt-content">
                    {!! $new->content !!}
                </div>

                <div class="tinlienquan">
                    <div class="i-title-sub">TIN TỨC KHÁC</div>
                    <ul class="row list-tinkhac">
                        @if (!empty($news))
                            @foreach($news as $key => $new)
                                <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                                    <a href="{{ route('promotionInfo.index', ['id' => $new->slug]) }}" title="{{ $new->title }}">{!! str_limit($new->title, $limit = 70, $end = '...') !!}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--NEW-->
    <div class="container n3-tour-hour">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title mg-bot10">
                    <a href="">Tour liên quan</a>
                </h2>
            </div>
            <div id="TourHot">
                @if (!empty($products))
                    @foreach($products->shuffle() as $key => $product)
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                            <a href="/{{ $product->category->title_seo."/".$product->title_seo  }}" title="{{ $product->name }}">
                                <div class="pos-relative">
                                    <img src="{{ $product->image }}" class="img-responsive pic-tgc" alt="{{ $product->name }}">
                                    <div class="frame-tgc1">
                                        <div class="row">
                                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                <div class="f-left">
                                                    <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                                </div>
                                                <div class="f-left date"><span class="yellow">{{ date('d/m/Y', strtotime($product->date_start)) }}</span> - <span class="yellow">{{ $product->sum_time }}</span></div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="frame-tgc2">
                                    <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">{!! str_limit($product->name, $limit = 40, $end = '...') !!}</div>
                                    <div class="tgc-line"></div>
                                    <div class="mg-bot10">
                                        <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                        <div class="f-left tgc-info">
                                            @if ($product->price > 0 && $product->price_sale > 0)
                                                <span class="price-o">{{ number_format($product->price, 0, '', ',') }} đ</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="price-n">{{ number_format($product->price_sale, 0, '', ',') }} đ</span>
                                            @elseif($product->price > 0 && $product->price_sale == 0)
                                                <span class="price-n">{{ number_format($product->price, 0, '', ',') }} đ</span>
                                            @elseif($product->price == 0)
                                                <span class="price-o">Đang cập nhật</span>
                                            @endif
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>



</div>
@endsection
