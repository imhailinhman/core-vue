@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="{{ route('home.index') }}" itemprop="url"><span itemprop="title">Trang chủ</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="faq">
        <div class="container n3-contact mg-bot40">
            <div class="row">
                <div class="col-xs-12 mg-bot15">
                    <div class="title mg-bot15"><h1>Câu hỏi thường gặp</h1></div>
                </div>

                <div class="faq-right col-sm-12">
                    <div id="accordion">
                        @if (!empty($faqs))
                            @foreach($faqs as $key => $faq)
                                <h3 class="mb-5">{{ $faq->title }}</h3>
                                <div>
                                    {!! $faq->description !!}
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div>

            </div>
        </div>
    </section>


</div>
@endsection

@section('js_frontend')
    <script>
        $(function () {
            $("#accordion").accordion({
                heightStyle: "content"
            });
        });
    </script>
@endsection
