<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/aa', function(){
    echo 111;
});

Route::group(['prefix' => '/v1', 'namespace' => 'V1', 'as' => 'api.'], function () {
    Route::resource('news', 'NewsController');
});

