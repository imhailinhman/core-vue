<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::auth();
Route::group(['middleware' => ['auth']], function () {

    // Route of admin
    Route::get('/admins/', ['as' => 'admins.index', 'uses' => 'Backend\AdminController@index']);
    Route::get('admins/create', ['as' => 'admins.create', 'uses' => 'Backend\AdminController@create']);
    Route::post('/admins', ['as' => 'admins.store', 'uses' => 'Backend\AdminController@store']);
    Route::put('admins/bulk-actions', ['as' => 'admins.bulkActions', 'uses' => 'Backend\AdminController@bulkActions']);
    Route::put('admins/item-actions', ['as' => 'admins.itemActions', 'uses' => 'Backend\AdminController@itemActions']);
    Route::get('admins/{admins}/edit', ['as' => 'admins.edit', 'uses' => 'Backend\AdminController@edit']);
    Route::post('admins/{admins}', ['as' => 'admins.update', 'uses' => 'Backend\AdminController@update'])->where('admins', '[0-9]+');;
    Route::post('/admins/update-device', ['as' => 'admins.update_device', 'uses' => 'Backend\AdminController@updateDevice']);
    Route::get('/{admins}/admins', ['as'=> 'admins.show', 'uses' => 'Backend\AdminController@show']);
    Route::get('/{admins}/changePassword', ['as'=> 'admins.changepassword', 'uses' => 'Backend\AdminController@changePassword']);
    Route::post('/{admins}/postchangePassword', ['as'=> 'admins.postchangePassword', 'uses' => 'Backend\AdminController@postChangePassword']);


    // Route of users
    Route::get('/users', ['as' => 'users.index', 'uses' => 'Backend\UserController@index']);
    Route::get('users/create', ['as' => 'users.create', 'uses' => 'Backend\UserController@create']);
    Route::post('/users', ['as' => 'users.store', 'uses' => 'Backend\UserController@store']);
    Route::put('users/bulk-actions', ['as' => 'users.bulkActions', 'uses' => 'Backend\UserController@bulkActions']);
    Route::put('users/item-actions', ['as' => 'users.itemActions', 'uses' => 'Backend\UserController@itemActions']);
    Route::get('users/{users}/edit', ['as' => 'users.edit', 'uses' => 'Backend\UserController@edit']);
    Route::post('users/{users}', ['as' => 'users.update', 'uses' => 'Backend\UserController@update'])->where('users', '[0-9]+');;
    Route::get('/{users}/users', ['as'=> 'users.show', 'uses' => 'Backend\UserController@show']);
    Route::get('users/districts', ['as'=> 'users.districts', 'uses' => 'Backend\UserController@districts']);

    // Route of roles
    Route::get('role', 'Backend\RoleController@index')->name('role.index');
    Route::get('role/detail', 'Backend\RoleController@getInfo')->name('role.get_info');
    Route::post('role', 'Backend\RoleController@create')->name('role.create');
    Route::post('role/update', 'Backend\RoleController@update')->name('role.update');
    Route::put('role/destroy', 'Backend\RoleController@destroy')->name('role.destroy');

    // Route of categories
    Route::get('/categories', ['as' => 'categories.index', 'uses' => 'Backend\CategoryController@index']);
    Route::get('categories/create', ['as' => 'categories.create', 'uses' => 'Backend\CategoryController@create']);
    Route::post('/categories', ['as' => 'categories.store', 'uses' => 'Backend\CategoryController@store']);
    Route::put('categories/bulk-actions', ['as' => 'categories.bulkActions', 'uses' => 'Backend\CategoryController@bulkActions']);
    Route::put('categories/item-actions', ['as' => 'categories.itemActions', 'uses' => 'Backend\CategoryController@itemActions']);
    Route::get('categories/{categories}/edit', ['as' => 'categories.edit', 'uses' => 'Backend\CategoryController@edit']);
    Route::post('categories/{categories}', ['as' => 'categories.update', 'uses' => 'Backend\CategoryController@update'])->where('users', '[0-9]+');;
    Route::get('/{categories}/categories', ['as'=> 'categories.show', 'uses' => 'Backend\CategoryController@show']);

    // Route of categories
    Route::get('/news', ['as' => 'news.index', 'uses' => 'Backend\NewController@index']);
    Route::get('news/create', ['as' => 'news.create', 'uses' => 'Backend\NewController@create']);
    Route::post('/news', ['as' => 'news.store', 'uses' => 'Backend\NewController@store']);
    Route::put('news/bulk-actions', ['as' => 'news.bulkActions', 'uses' => 'Backend\NewController@bulkActions']);
    Route::put('news/item-actions', ['as' => 'news.itemActions', 'uses' => 'Backend\NewController@itemActions']);
    Route::get('news/{news}/edit', ['as' => 'news.edit', 'uses' => 'Backend\NewController@edit']);
    Route::post('news/{news}', ['as' => 'news.update', 'uses' => 'Backend\NewController@update'])->where('news', '[0-9]+');;
    Route::get('/{news}/news', ['as'=> 'news.show', 'uses' => 'Backend\NewController@show']);

    // Route of product
    Route::get('/products', ['as' => 'products.index', 'uses' => 'Backend\ProductController@index']);
    Route::get('products/create', ['as' => 'products.create', 'uses' => 'Backend\ProductController@create']);
    Route::post('/products', ['as' => 'products.store', 'uses' => 'Backend\ProductController@store']);
    Route::put('products/bulk-actions', ['as' => 'products.bulkActions', 'uses' => 'Backend\ProductController@bulkActions']);
    Route::put('products/item-actions', ['as' => 'products.itemActions', 'uses' => 'Backend\ProductController@itemActions']);
    Route::post('product/delete-image/{products}', ['as' => 'products.deleteProductImaget', 'uses' => 'Backend\ProductController@deleteProductImage']);
    Route::post('product/upload-image', ['as' => 'products.updateProductImage', 'uses' => 'Backend\ProductController@updateProductImage']);

    Route::get('products/{products}/edit', ['as' => 'products.edit', 'uses' => 'Backend\ProductController@edit']);
    Route::post('products/{products}', ['as' => 'products.update', 'uses' => 'Backend\ProductController@update'])->where('users', '[0-9]+');;
    Route::get('/{products}/products', ['as'=> 'products.show', 'uses' => 'Backend\ProductController@show']);

    // Route of product
    Route::get('/menus', ['as' => 'menus.index', 'uses' => 'Backend\MenuController@index']);
    Route::get('menus/create', ['as' => 'menus.create', 'uses' => 'Backend\MenuController@create']);
    Route::post('/menus', ['as' => 'menus.store', 'uses' => 'Backend\MenuController@store']);
    Route::put('menus/bulk-actions', ['as' => 'menus.bulkActions', 'uses' => 'Backend\MenuController@bulkActions']);
    Route::put('menus/item-actions', ['as' => 'menus.itemActions', 'uses' => 'Backend\MenuController@itemActions']);
    Route::get('menus/{menus}/edit', ['as' => 'menus.edit', 'uses' => 'Backend\MenuController@edit']);
    Route::post('menus/{menus}', ['as' => 'menus.update', 'uses' => 'Backend\MenuController@update'])->where('menus', '[0-9]+');;
    Route::get('/{menus}/menus', ['as'=> 'menus.show', 'uses' => 'Backend\MenuController@show']);

    // Route of slide
    Route::get('/slides', ['as' => 'slides.index', 'uses' => 'Backend\SlideController@index']);
    Route::get('slides/create', ['as' => 'slides.create', 'uses' => 'Backend\SlideController@create']);
    Route::post('/slides', ['as' => 'slides.store', 'uses' => 'Backend\SlideController@store']);
    Route::put('slides/bulk-actions', ['as' => 'slides.bulkActions', 'uses' => 'Backend\SlideController@bulkActions']);
    Route::put('slides/item-actions', ['as' => 'slides.itemActions', 'uses' => 'Backend\SlideController@itemActions']);
    Route::get('slides/{slides}/edit', ['as' => 'slides.edit', 'uses' => 'Backend\SlideController@edit']);
    Route::post('slides/{slides}', ['as' => 'slides.update', 'uses' => 'Backend\SlideController@update'])->where('slides', '[0-9]+');;
    Route::get('/{slides}/slides', ['as'=> 'slides.show', 'uses' => 'Backend\SlideController@show']);


    // Route of categories
    Route::get('/faq', ['as' => 'faq.index', 'uses' => 'Backend\FaqController@index']);
    Route::get('faq/create', ['as' => 'faq.create', 'uses' => 'Backend\FaqController@create']);
    Route::post('/faq', ['as' => 'faq.store', 'uses' => 'Backend\FaqController@store']);
    Route::put('faq/item-actions', ['as' => 'faq.itemActions', 'uses' => 'Backend\FaqController@itemActions']);
    Route::get('faq/{faq}/edit', ['as' => 'faq.edit', 'uses' => 'Backend\FaqController@edit']);
    Route::post('faq/{faq}', ['as' => 'faq.update', 'uses' => 'Backend\FaqController@update'])->where('faq', '[0-9]+');;
    Route::get('/{faq}/faq', ['as'=> 'faq.show', 'uses' => 'Backend\FaqController@show']);

    // Route of categories
    Route::get('/contacts', ['as' => 'contacts.index', 'uses' => 'Backend\ContactController@index']);
    Route::get('contacts/create', ['as' => 'contacts.create', 'uses' => 'Backend\ContactController@create']);
    Route::post('/contacts', ['as' => 'contacts.store', 'uses' => 'Backend\ContactController@store']);
    Route::put('contacts/bulk-actions', ['as' => 'contacts.bulkActions', 'uses' => 'Backend\ContactController@bulkActions']);
    Route::put('contacts/item-actions', ['as' => 'contacts.itemActions', 'uses' => 'Backend\ContactController@itemActions']);
    Route::get('contacts/{contacts}/edit', ['as' => 'contacts.edit', 'uses' => 'Backend\ContactController@edit']);
    Route::post('contacts/{faq}', ['as' => 'contacts.update', 'uses' => 'Backend\ContactController@update'])->where('faq', '[0-9]+');;
    Route::get('/contacts/{contacts}', ['as'=> 'contacts.show', 'uses' => 'Backend\ContactController@show']);


    // Route of visa
    Route::get('/visas', ['as' => 'visas.index', 'uses' => 'Backend\VisaController@index']);
    Route::get('visas/create', ['as' => 'visas.create', 'uses' => 'Backend\VisaController@create']);
    Route::post('/visas', ['as' => 'visas.store', 'uses' => 'Backend\VisaController@store']);
    Route::put('visas/bulk-actions', ['as' => 'visas.bulkActions', 'uses' => 'Backend\VisaController@bulkActions']);
    Route::put('visas/item-actions', ['as' => 'visas.itemActions', 'uses' => 'Backend\VisaController@itemActions']);
    Route::get('visas/{visas}/edit', ['as' => 'visas.edit', 'uses' => 'Backend\VisaController@edit']);
    Route::post('visas/{visas}', ['as' => 'visas.update', 'uses' => 'Backend\VisaController@update'])->where('visas', '[0-9]+');;
    Route::get('/{visas}/visas', ['as'=> 'visas.show', 'uses' => 'Backend\VisaController@show']);

});
